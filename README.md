# _MC Augmentor - Sequoia Trees_ | A Minecraft Datapack!

![Sequoia Trees Picture](pack.png)

## Current Datapack Version: 1.19-02

This datapack was created to provide Sequoia trees to Minecraft Java Edition.  
It currently supports Minecraft versions 1.18.X - 1.19 (though 1.18.X complains about the datapack version, it should still work).
It replaces all Old Growth _Spruce_ Taiga trees with Sequoia trees (it does not affect Old Growth _Tree_ Taiga).

This is a World Generation change datapack, therefore it requires experimental settings.

> NOTE: These trees (like most generated things in Minecraft) are not perfect, while the vast majority generate without issues, you may get occasional oddities - I don't feel there is much value in fixing these, as the generation sometimes looks quite cool, like remnant "tree stumps" or similar.

_______

## What does this datapack do?

It simply replaces the Old Growth Spruce Taiga 'trees' with custom generated Sequoia's.

The Sequoia's are not generated as explicit minecraft trees, they are generated with custom "random_patch" methods for multiple types of interwoven trees with varying heights and attributes.

They are generated with mixes of dark_oak and oak logs, with spruce and oak leaves in the following ratios:

- 1:1 dark_oak:oak ratio of _logs_ in the floor 'root system'
- 1:5 dark_oak:oak ratio of _logs_ going up the trunk into the foilage
- 1:5 spruce:oak ratio of _leaves_ going up the trunk into the foilage

The Spruce and Oak leaves will naturally degenerate - dropping standard drops for Spruce and Oak leaves respectively.

It has no effect on any other biome than the 'Old Growth Spruce Taiga' (previously known as 'Giant Spruce Taiga')

**That's it!**

## Oh yes... How do you install it?

It is a generic Minecraft datapack, nothing out of the ordinary from the standard minecraft instructions.

Here's Mojang's official guide for all Operating Systems: [Loading a datapack](https://minecraft.fandom.com/wiki/Tutorials/Installing_a_data_pack)

Just [Download The Pack Here](https://www.planetminecraft.com/data-pack/mc-augmentor-sequoia-trees/)

**... OR ...**

ZIP everything up from this project into a single zip file (Something like *MC-Augmentor_Sequoia-Trees.zip* if, like me, you're not feeling more creative :P).  

Then place the zip file in your minecraft save game `datapack` folder (preferably before you load the world for the first time, otherwise nearby chunks may not see the effects).

WARNING: If you happen to use the "Download Source Code" with *ZIP* option at the top of this page (Next to the green CLONE button), it will put the contents inside a folder that is _then_ zipped ... that is incorrect, Minecraft expects the files in this project to be directly in the Zip file, not in a folder that is then zipped.


